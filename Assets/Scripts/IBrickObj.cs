using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBrickObj
{

    void SetProp(Brick brick);

    void DestroyBrick();

    int GetPrice();

}
