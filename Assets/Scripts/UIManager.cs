using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIManager : MonoBehaviour
{
    private static UIManager _instance;
    public static UIManager Instance => _instance;


    public TextMeshProUGUI healthText;
    public TextMeshProUGUI scoreText;


    private void Awake()
    {
        _instance = this;
    }

    public void Start()
    {
        Ball.onChangeHeahs += Ball_onChangeHeahs;
        Ball.onChangeScore += Ball_onChangeScore;
    }

    private void Ball_onChangeScore(int change)
    {
        scoreText.text = "Score: " + change.ToString();
    }

    private void Ball_onChangeHeahs(int change)
    {
        healthText.text = "Health: " + change.ToString();
    }

}
