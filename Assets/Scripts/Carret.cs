using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Carret : MonoBehaviour
{
    private const float Ypos = -4.49f;
    // Start is called before the first frame update
    void Start()
    {
        GlobalEventsController.onSlide += Player_onSlide;
    }

    /// <summary>
    /// ��������� ������� ������ �������
    /// </summary>
    /// <param name="x">Vector2.x</param>
    private void Player_onSlide(float x)
    {
        transform.position = new Vector2(x, Ypos);
    }

}
