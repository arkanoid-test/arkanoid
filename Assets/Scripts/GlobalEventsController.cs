using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalEventsController : MonoBehaviour
{
    public delegate void OnSlide(float progress);
    public static event OnSlide onSlide;



    [SerializeField]
    private float velocity;


    private float x;

    [SerializeField]
    private float limit;
    void Update()
    {
        if (Input.anyKey)
        {
            x += Input.GetAxis("Horizontal") * velocity *Time.deltaTime;

            x = Mathf.Clamp(x, -limit, limit);

            onSlide.Invoke(x);
        }
    }

}
