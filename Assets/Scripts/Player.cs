using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{



    [SerializeField] private Vector3 startingBallPosition;
    [SerializeField] private Ball ballPrefab;

    private IBall ball;

    // Start is called before the first frame update
    void Start()
    {
        // �������� ������ � ������ �����
        var ballOjbect = Instantiate(ballPrefab, startingBallPosition, Quaternion.identity);
        ball = ballOjbect.GetComponent<IBall>();
        ball.ResetVelocity();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKey)
        {
            if (Input.GetKeyDown(KeyCode.R)) // ���������� ������ �� ������
            {
                ball.SetPosition(startingBallPosition);
                ball.ResetVelocity();
            }
        }
    }


}
