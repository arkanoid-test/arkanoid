using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickGenerator : MonoBehaviour
{
    public List<Brick> BricksParameters;
    public BrickObj brickObjPrefab;
    private int colums = 5;
    [SerializeField] private int MaxRows;
    [SerializeField] private float yStep = 0.2f;
    [SerializeField] private Vector2 startPosition;
    public void Start()
    {
        for(int i= 0;i<colums;i++)
        {
            int randRows = Random.Range(1, MaxRows);
            for (int j = 0; j < randRows; j++)
            {
                IBrickObj brickObj = Instantiate(brickObjPrefab, new Vector3(startPosition.x + i, startPosition.y - (yStep * j)), Quaternion.identity);
                brickObj.SetProp(BricksParameters[Random.Range(0, BricksParameters.Count)]);
            }
        }
    }
}
