using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickObj : MonoBehaviour, IBrickObj
{
    // Start is called before the first frame update
    private Brick properties;
    private Brick Properties
    {
        get => properties;
        set { properties = value; spriteRenderer.color = value.color; }
    }
    [SerializeField] private SpriteRenderer spriteRenderer;
    public void DestroyBrick()
    {
        Destroy(gameObject);
    }

    public int GetPrice()
    {
        return Properties.price;
    }

    public void SetProp(Brick brick)
    {
        Properties = brick;
    }


}
