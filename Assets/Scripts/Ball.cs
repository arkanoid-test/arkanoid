using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Collisions
{
    DeathZone,
    Brick
}

public class Ball : MonoBehaviour, IBall
{
    [SerializeField] private Vector2 startingVelocity; // ����������� ��� ������
    [SerializeField] private Rigidbody2D rb;
    [SerializeField]
    private int health;
    private int score;
    private Vector2 startPosition; 
    public delegate void OnChangeHeahs(int change);
    public static event OnChangeHeahs onChangeHeahs;
    public delegate void OnChangeScore(int change);
    public static event OnChangeScore onChangeScore;

    /// <summary>
    /// ������ ������
    /// </summary>
    public void ResetVelocity()
    {
        rb.velocity = Vector2.zero;
     
        rb.AddForce(startingVelocity, ForceMode2D.Impulse);
    }

    /// <summary>
    /// ��������� ������� �������
    /// </summary>
    /// <param name="position"></param>
    public void SetPosition(Vector3 position)
    {
        startPosition = transform.position = position;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag(Collisions.DeathZone.ToString()))
        {
            health--;
            onChangeHeahs.Invoke(health);
            SetPosition(startPosition);
            ResetVelocity();
        }
        else if (collision.transform.CompareTag(Collisions.Brick.ToString()))
        {
            IBrickObj brickObj = collision.gameObject.GetComponent<IBrickObj>();
            int price = brickObj.GetPrice();
            score += price;
            onChangeScore.Invoke(score);
            brickObj.DestroyBrick();
        }
    }

    public void Start()
    {
        onChangeHeahs.Invoke(health); // ��������� ���������� ������
    }
}
