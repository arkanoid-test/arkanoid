using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBall
{
    void ResetVelocity();
    void SetPosition(Vector3 position);
}
