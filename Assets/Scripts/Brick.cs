using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Brick
{
    public Color32 color;
    public int price;
    public int heals;
}
